<?php
/*
|-----------------------------------
| USER DEFINED VARIABLES
|-----------------------------------
|
|
| This is a set of variables that are made specific to this app.
| These are placed here to maintain security and ease of use.
| Use config( 'your_key' ); or Config::get( 'constraints.key' ) to get the values.
| in a view use {{ config( 'constraints.key' ) }}
|
|
*/
return [
  "debug"                => true,                                // True to display debug messages. Should be disabled in production environment.
  "custom_qualtrics_url" => "https://google.com",  // The base url for Qualtrics surveys to address.
  "custom_survey_id"     => "2345",                // The (default) survey to address. Demonstration without grading callback.
#"custom_survey_id"	=> "SV_7WzVMLISwePDaxT",		// The (default) survey to address. Demonstration for grading callback.
  #"custom_survey_id"     => "SV_bO9IijOwGYyAXzL",                // The (default) survey to address.
  "custom_pass_params"   => array(                               // The parameters that should be passed from the launch request to Qualtrics.
      "user_id",
      "lis_result_sourcedid" // Required for grading callbacks.
  ),
  "custom_pass_all"      => false,                               // True to ignore the custom_pass_params value and pass all parameters to Qualtrics.
  "allowUrlOverrides"    => true,                                // Whether or not Tool Consumers are allowed to override custom_qualtrics_url by specifying a custom value.
  "allowIdOverrides"     => true,                                // Whether or not Tool Consumers are allowed to override custom_survey_id by specifying a custom value.
  "consumerSecrets"      => array(                               // Consumer secrets for authentication. These should be kept private!
      "consumerKey"               => "lms-1",
      "consumerSecret"            => "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
      "LTI_Bridge_Demonstration"  => "powertotheteachers",
      "LTI_Bridge_Development"    => "powertotheresearchers"
  ),
  "provideGrading"       => true,                                // SEE README FOR IMPORTANT INFORMATION REGARDING GRADING.
  "verifyGuzzleRequests" => true,
  'course-urls' => array(
    'lsm'   => 'https://lm.cals.wgu.edu/lms',
    'maar'  => 'https://ma.cals.wgu.edu/maar',
    'tc'    => 'https://tc.cals.wgu.edu/tc',
    'mm'    => 'https://mm.cals.wgu.edu/mm'
  ),
];
