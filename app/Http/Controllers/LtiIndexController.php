<?php
namespace App\Http\Controllers;
// LTI class; contains the main logic for the tool.

use App\Http\Controllers\Oauth\OauthController;
use App\Http\Controllers\Oauth\ConsumerSecrets;
use Illuminate\Support\Facades\Storage;
// use App\Http\Controllers\LtiController;

require_once "LtiController.php";
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class LtiIndexController extends Controller {

  public function home() {
    return view( 'welcome' );
  }

  public function index() {
    ob_start();
    if (config( "constants.debug" ) ) {
        //ini_set('display_startup_errors', 1);
        //ini_set('display_errors', 1);
        //error_reporting(-1);
    }
    /**
     * This is the default entry point for the tool. It should be specified as the LTI endpoint for the Tool Consumer.
     */
    // https://urls.com/thispage.php?activity=1&id=2&email=this@this.com
    try {
      if($_SERVER['REQUEST_METHOD' ] == 'GET' ) {
        echo '-----';
        $headers = array();
        $entityBody = file_get_contents( 'php://input' );

        foreach ($_SERVER as $name => $value) {
              if (substr($name, 0, 5) == 'HTTP_') {
                  $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
                  $headers[$name] = $value;
              } else if ($name == "CONTENT_TYPE") {
                  $headers["Content-Type"] = $value;
              } else if ($name == "CONTENT_LENGTH") {
                  $headers["Content-Length"] = $value;
              }
          }
        echo '<pre>';
        echo 'THESE ARE HEADERS!!!!<br><br>';
        print_r( $headers );
        http_response_code(200);
      }

        if (!is_writable(session_save_path()))
            die('Session path "' . session_save_path() . '" is not writable for PHP.');

        session_start();

        // Create the OAuth data store holding consumer secrets. All secrets defined in the configuration are added to the data store.

        $secrets = new ConsumerSecrets;

        foreach (config("constants.consumerSecrets") as $key => $value)
            $call = $secrets->set_consumer($key, $value);

        // Create an instance of the LTI class, using the POST (or GET) parameters of the request as launch parameters.

        $launchParams = $_REQUEST;

        if (!config("constants.allowUrlOverrides") || empty($launchParams["custom_qualtrics_url"]))
            $launchParams["custom_qualtrics_url"] = config("constants.custom_qualtrics_url");

        if (!config("constants.allowIdOverrides") || empty($launchParams["custom_survey_id"]))
            $launchParams["custom_survey_id"] = config("constants.custom_survey_id");

        $lti = new LtiController(

            $launchParams,  // Pass the launch parameters for the LTI request.
            $secrets        // Pass the collection of consumers that can be authenticated.
        );

        echo hash_hmac('ripemd160', 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 'secret' );
        echo '<pre> Launch Params <br><br>';
        print_r( $launchParams );

        echo '<br><br> Secrets <br><br>';
        print_r( $secrets );

        // exit();
        // $results = config;
       // var_dump( $results )

        // 1. Validate the launch request.

        if ($lti->isValidLaunchRequest()) {
            // 2. Identify the user.
            echo 'THIS IS TRUE LINE 109';
            if (!$lti->isAuthenticated()) {
                echo 'THIS IS AUTHENTICATED';
                // The request didn't pass OAuth authentication.
                // Set the HTTP response to 402 (Unauthorized) and stop script execution.
                // It's the Tool Consumer's responsibility to handle the response code.

                http_response_code(402);
                exit("Launch request could not be authorized.");
            }
            // 3. Register a session to perform the grading callback if allowed and supported.

            if (config("constants.provideGrading"))
                $lti->tryRegisterCallbackSession();

            // 4. Launch the learning tool.
            $lti->launch();

            // A HTTP redirect has been performed and this code is never executed. This is the end of script execution.
        } else if ($lti->isValidGradingCallback()) {

            // The request is a grading callback from Qualtrics.

            echo "Your result has been received from Qualtrics. This means everything went fine :)" . "<br />";

            if (config("constants.provideGrading") && $lti->tryPerformGradingCallback(true)) {

                echo "We have successfully recorded your grade.<br />";
                // echo '<pre>';
                // var_dump( $lti );
            }

            echo "You can now close this window.";
        } else {

            // The Tool Consumer made an invalid LTI request.
            // Set the HTTP response to 400 (Bad Request) and stop script execution.
            // It's the Tool Consumer's responsibility to handle the response code.

            http_response_code(400);
            // echo '<pre>';
            // var_dump( $lti );
            exit("Not a valid LTI launch request.");
        }
    }
    catch (Exception $ex) {

        http_response_code(500);
        exit(config("constants.debug") ? $ex : "Oops, something went wrong on the server.");
    }
  }
}
