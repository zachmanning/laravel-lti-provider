<?php
namespace App\Http\Controllers\Oauth;

class OAuthDataStoreController {

  private $consumer_key = NULL;
  private $consumer_secret = NULL;

  function lookup_consumer($consumer_key) {
    // implement me
    echo 'Consumer Key : ' . $this->consumer_key;
    exit();
    return new OAuthConsumer( $this->consumer_key, $this->consumer_secret );
  }

  function lookup_token($consumer, $token_type, $token) {
    // implement me
    return new OAuthToken( $consumer, '' );
  }

  function lookup_nonce($consumer, $token, $nonce, $timestamp) {
    // implement me
    // If a persisten store is available nonece values should be retained for a period and checked here
    return FALSE;
  }

  function new_request_token($consumer, $callback = null) {
    // return a new token attached to this consumer
    return NULL;
  }

  function new_access_token($token, $consumer, $verifier = null) {
    // return a new access token attached to this consumer
    // for the user associated with this token if the request token
    // is authorized
    // should also invalidate the request token
    return NULL;
  }

}
